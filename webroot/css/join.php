<?php
public function getRestaurantData($radius, $lat, $long, $page, $limit, $category_id) {
        $providers = array();
        // formuala to calculate to get radius distance between latitudes and longitudes with provided lat,long values 
        $distanceField = '(6371 * acos (cos ( radians(:latitude) )
                            * cos( radians( latitude ) )
                            * cos( radians( longitude )
                            - radians(:longitude) )
                            + sin ( radians(:latitude) )
                            * sin( radians( latitude ) )))';

        $condition = ['Users.user_type' => 3];

            //$jCondition = [];
        if (!empty ($category_id)) {
           $condition = ['MenuItems.category_id' => 1];
            //$condition['MenuItems.category_id IN (1,2)'];
        }

       /* $usersModel = TableRegistry::get('Users');
        $menuModel = TableRegistry::get('MenuItems');
        $data = $menuModel->find('all')->contain(['MenuCategories', 'Users' => function(\Cake\ORM\Query $q) use ($condition) {
                    return $q->where($condition);
                }
            ])->where($jCondition)->toArray();
        pr($data);die;*/
        //$userarr = $usersModel->find('all')->contain(['MenuItems'])->toArray();
        //echo '<pre>'; print_r($userarr); die;
        $usersModel = TableRegistry::get('Users');
            $providers = $usersModel->find()
            ->select(['Users.id', 'Users.email', 'Users.user_type', 'UserProfiles.user_id', 'UserProfiles.latitude', 'UserProfiles.longitude', 'UserProfiles.profile_image', 'UserProfiles.restaurant_name', 'UserProfiles.restaurant_tag', 'MenuItems.category_id', 'MenuItems.item_name', 'MenuItems.choice', 'MenuCategories.name', 'distance' => $distanceField])
            ->join([
                'userprofiles' => [
                'table' => 'user_profiles',
                'type' => 'INNER',
                'conditions' => ['UserProfiles.user_id = Users.id'],
                'alias' => 'UserProfiles'],
                 'menuitems' => [
                        'table' => 'menu_items',
                        'type' => 'INNER',
                        'conditions' => ['MenuItems.user_id = Users.id'],
                        'alias' => 'MenuItems'
                    ],
                  'menucategories' => [
                        'table' => 'menu_categories',
                        'type' => 'INNER',
                        'conditions' => ['MenuItems.category_id = MenuCategories.id'],
                        'alias' => 'MenuCategories'
                    ],
            ])
            ->where($condition)->having(["distance < " => $radius])  
            ->order(['distance'])
            ->limit($limit)
            ->page($page)
            ->bind(':latitude', $lat, 'float')
            ->bind(':longitude', $long, 'float')
            ->toArray();
            echo '<pre>'; print_r($providers); die;
            // print_r($jcondition); die;
            /*$providers = $usersModel->find('all')->select(['Users.id', 'Users.email', 'Users.user_type', 'distance' => $distanceField])->contain(['UserProfiles', 
                'MenuItems' => function(\Cake\ORM\Query $q) use ($jCondition) {
                    return $q->where($jCondition)->contain(['MenuCategories']);
                }
            ])->where($condition)->having(["distance < " => $radius])  
            ->order(['distance'])
            ->limit($limit)
            ->page($page)
            ->bind(':latitude', $lat, 'float')
            ->bind(':longitude', $long, 'float')->toArray();*/
             /*$providers = $usersModel->find()
            ->select(['Users.id', 'Users.email', 'Users.user_type', 'UserProfiles.user_id', 'UserProfiles.latitude', 'UserProfiles.longitude', 'UserProfiles.profile_image', 'UserProfiles.restaurant_name', 'UserProfiles.restaurant_tag', 'distance' => $distanceField])
             ->contain(['UserProfiles', 'MenuItems' => function(\Cake\ORM\Query $q) use ($jCondition) {
                    return $q->where($jCondition)->contain(['MenuCategories']);
                }
            ])*/
             /*->matching('MenuItems', function ($q) use($jCondition) {
                return $q->where($jCondition);
            })*/
            /*->where($condition)->having(["distance < " => $radius])  
            ->order(['distance'])
            ->limit($limit)
            ->page($page)
            ->bind(':latitude', $lat, 'float')
            ->bind(':longitude', $long, 'float')
            ->toArray();*/
            //echo '<pre>'; print_r($providers); die;
            return $providers;
        }